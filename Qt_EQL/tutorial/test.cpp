#include "test.h"
#include <QtDebug>
#include <eql5/eql_fun.h>

// class Test

Test::Test(QObject* parent, const QString& name) : QObject(parent) {
    setObjectName(name);
}

QObject* Test::newInstance(QObject* parent, const QString& name) {
    return new Test(parent, name);
}

QString Test::concat(const QStringList& list) {
    return list.join(", ");
}

void Test::processData(cl_object data) {
    // meant for passing complex Lisp data to be processed in C++

    if(cl_listp(data) == ECL_T) {
        cl_object l_dolist = data;
        while(l_dolist != ECL_NIL) {
            cl_print(1, cl_car(l_dolist));
            l_dolist = cl_cdr(l_dolist);
        }
        cl_terpri(0);
    }
}

void Test::printMe() {
    // you may pass up to 10 arguments of any type found in
    // '~/eql5/src/ecl_fun.cpp::toMetaArg()', wrapped in macro Q_ARG;
    // C++ class instances are passed as pointers of a vanilla Qt class
    // known to EQL5, here: 'QObject*'

    eql_fun("eql-user:print-qt-object", Q_ARG(QObject*, this));
}

// class Test2, which inherits Test

Test2::Test2(QObject* parent, const QString& name) : Test(parent) {
    setObjectName(name);
}

QObject* Test2::newInstance(QObject* parent, const QString& name) {
    return new Test2(parent, name);
}

void Test2::printAllMemberFunctions() {
    // see comment above
    eql_fun("eql:qapropos", Q_ARG(bool, false),
                            Q_ARG(QObject*, this));
}
