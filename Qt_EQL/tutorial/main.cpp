#include "test.h"
#include <eql5/eql.h>
#include <QApplication>
#include <QLabel>

int main(int argc, char* argv[]) {
    QApplication app(argc, argv);

    QLabel* main = new QLabel("<h2>Main Window</h2>");
    main->setAlignment(Qt::AlignCenter);
    main->resize(600, 400);
    main->show();

    EQL eql;
    EQL::eval("(in-package :eql-user)");

    // add desired Qt class instances as Lisp variables (uses 'defvar');
    // you may provide a package name (which needs to exist); if not provided,
    // the current package will be used (see above 'in-package');
    // pass 'true' as last argument to also call 'define-qt-wrappers'

    EQL::addObject(main, "eql-user:*main-widget*");           // add main

    // note argument 3 (true): 'define-qt-wrappers'
    EQL::addObject(new Test(main), "*test*", true);           // add 'Test'

    // note argument 4 (false): do not lispify C function names
    EQL::addObject(new Test2(main), "*test-2*", true, false); // add 'Test2'

    EQL::eval("(load \"test.lisp\")"); // will start a REPL
    app.processEvents();               // needed for 'qlater' in 'test.lisp'

    return 0;                          // no 'app.exec()' because of REPL
}

